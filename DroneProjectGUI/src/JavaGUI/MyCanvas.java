package JavaGUI;

/**
 *  this class defines the canvas for the GUI overlay
 */

import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.scene.text.TextAlignment;

public class MyCanvas {
	double xCanvasSize = 512;			
	double yCanvasSize = 512; // set dimensions
    GraphicsContext gc; 

    /**
     * constructor to set our attributes
     * @param g
     * @param cs
     */
    public MyCanvas(GraphicsContext g, double d, double e) {
    	gc = g;
    	xCanvasSize = d;
    	yCanvasSize = e;
    }
    
    /**
     * simply clears the canvas
     * 
     */
    
    public void clearCanvas() {
		gc.clearRect(0,  0,  xCanvasSize,  yCanvasSize);		// for clearing event
    }
	
	/** 
	 * function to convert char c to actual colour used
	 * @param c
	 * @return Color
	 */
	Color colFromChar (char c){
		Color ans = Color.BLACK;
		switch (c) {
		case 'y' :	ans = Color.YELLOW;
					break;
		case 'w' :	ans = Color.WHITE;
					break;
		case 'r' :	ans = Color.RED;
					break;
		case 'g' :	ans = Color.GREEN;
					break;
		case 'b' :	ans = Color.BLUE;
					break;
		case 'o' :	ans = Color.ORANGE;
					break;
		}
		return ans;
	}
	
	public void setFillColour (Color c) {
		gc.setFill(c);
	}
	
	/**
	 * show the drone at position x,y and fill colour
	 * @param x
	 * @param y
	 * @param rad
	 * 
	 */

	public void showDrone(double x, double y, double rad) {
		gc.fillArc(x-rad, y-rad, rad*2, rad*2, 0, 360, ArcType.ROUND);	// within our limits 
	}

	/**
	 * show text function to write at given coords
	 * @param x
	 * @param y
	 * @param s
	 */
	public void showText (double x, double y, String s) {
		gc.setTextAlign(TextAlignment.CENTER);							
		gc.setTextBaseline(VPos.CENTER);								
		gc.setFill(Color.WHITE);										
	}

	/**
	 * show an int, by writing at given coords
	 * @param x
	 * @param y
	 * @param i
	 */
	
	public void showInt (double x, double y, int i) {
		showText (x, y, Integer.toString(i));
	}	
}