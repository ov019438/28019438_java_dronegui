package JavaGUI;

/**
 * DroneArena, keeps track of all entities within the arena and manipulates them when necessary
 * @author Derin Aya
 *

 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JFileChooser;

import javafx.scene.paint.Color;

/**
 * Constructor for DroneArena
 */
public class DroneArena {
	double xSize, ySize;								// define size
	double dx, dy;									
	private ArrayList<Entities> arrOfDrones;			// contains all entities in the arena
	Random randomGenerator = new Random();
	
	/**
	 * construct an arena with default params
	 */
	DroneArena() {
		this(900, 500);			// passes default params to itself
	}
	
	/**
	 * construct arena with given params
	 * @param xS double
	 * @param yS double
	 */
	DroneArena(double xS, double yS){
		xSize = xS;
		ySize = yS;
		arrOfDrones = new ArrayList<Entities>();					// define list for all entities
	}
	
	/**
	 * return arena size x
	 * @return
	 */
	
	public double getXSize() {
		return xSize;
	}
	/**
	 * return arena size y
	 * @return
	 */
	public double getYSize() {
		return ySize;
	}
	
	/**
	 * draws the entities onto the canvas
	 * @param gc
	 */
	
	public void drawArena(MyCanvas gc) {
		for (Entities d : arrOfDrones) {						
	 		if (d instanceof Drone) {
	 			gc.setFillColour(Color.RED); // sets a colour for the drones and obstacles
	 			d.drawDrone(gc);
	 		}
	 		else if (d instanceof Obstacle) {
	 			gc.setFillColour(Color.YELLOW);				
	 			d.drawDrone(gc);	// then draws them						
	 		}
	 	}		
	}
	
	/**
	 * runs checkDrone on all drones, adjusts angle if necessary
	 */
	
	public void checkDrones() {
		for (Entities ai : arrOfDrones) ai.checkDrone(this);	// check all drones
	}
	
	/**
	 * runs adjust on all drones, moves if necessary
	 */
	
	public void adjustDrones() {
		for (Entities ai : arrOfDrones) ai.adjustDrone();
	}
	 	
	/**
	 * return describes details of all drones in arena
	 * @return ans
	 */
	
	public ArrayList<String> describeAll() {
		ArrayList<String> ans = new ArrayList<String>();		// array list of type string to hold info on all drones
		for (Entities ai : arrOfDrones) ans.add(ai.toString());			// build array using info of all drones and obstacles
		return ans;												// return the built string
	}
	
	/** 
	 * Checks angle of a drone, if it hits a wall it needs to be bounced back
	 * @param x				x position
	 * @param y				y position
	 * @param rad			radius of drone
	 * @param ang			current angle
	 * @param notID			current drone's id (ignore if it's the current drone)
	 * @return				new angle 
	 */
	
	public double CheckDroneAngle(double x, double y, double rad, double ang, int notID) {
		double ans = ang;
		if (x < rad || x > xSize - rad) ans = 180 - ans;
			// collision on left or right side, mirror the direction of travel to opposite way
		if (y < rad || y > ySize - rad) ans = - ans;
			// hits roof or floor, do the same (mirror)
		
		for (Entities ai : arrOfDrones) 
			if (ai.getID() != notID && ai.collision(x, y, rad)) ans = 180*Math.atan2(y-ai.getY(), x-ai.getX())/Math.PI;
				// runs check on all drones for collision EXCEPT current drone (or else drone would get stuck)
				// if collision = true return angle between collisions
		
		return ans;		// return the angle
	}

	/**
	 * check if collision between drones
	 * @param drone	Entities
	 * @return 	true (if hit)
	 */
	
	public boolean checkHit(Entities drone) {
		boolean ans = false;
		for (Entities ai : arrOfDrones)
			if (ai instanceof Obstacle && ai.collision(drone)) { //run collision detection on all drones
				ans = true; 
			}
		return ans;
	}
	
	
	/**
	 * Takes a parameter for an arena entity to add, then adds it
	 * @param type char
	 * 
	 */
	
	public void addDrone(char type){
		dx = randomGenerator.nextInt((int) (xSize));	
		dy = randomGenerator.nextInt((int) (ySize));
		if(type == 'd'){
			arrOfDrones.add(new Drone(dx, dy, 10, 25, 5));
		}
		if(type == 'o'){
			arrOfDrones.add(new Obstacle(dx, dy, 15));
		}
	}

	/**
	 * method to clear the arena
	 */
	public void resetArena() {
		arrOfDrones.clear();		// clears the arena
	}
	
	/**
	 * method to increase drone speed
	 */
	
	public void increaseSpeed() {
		for(Entities d: arrOfDrones) {
			if(d instanceof Drone && ((Drone) d).speed >= 1) {
				((Drone) d).speed += 1;
			}
		}
	}
	
	/**
	 * method to decrease drone speed
	 */
	
	public void decreaseSpeed() {
		for(Entities d: arrOfDrones) {
			if(d instanceof Drone && ((Drone) d).speed >= 1) {
				((Drone) d).speed -= 1;
			}
		}
	}
	
	void saveFile() throws IOException { //*Selection*
		JFileChooser jFC = new JFileChooser("C:\\Users\\Derin\\eclipse-workspace");	//Set file directory
		int uSelect = jFC.showOpenDialog(null); //To receive user input
		if (uSelect == JFileChooser.APPROVE_OPTION) { //Approved when user has opened a file
			File userFile = jFC.getSelectedFile(); //jFC function to open the select file
			writeToFile(userFile); //Now write to file
		}
	}
    
    void writeToFile(File fToWrite) throws IOException { //*Saving*
    	FileWriter fileWriter = new FileWriter(fToWrite); //File buffer object to write to save file
		BufferedWriter buffer = new BufferedWriter(fileWriter); //Need to use buffer buffer for multiple writing statements for efficiency
		buffer.write(Integer.toString((int)xSize)); //Firstly write arena width and height to file
		buffer.write(","); //Separate by comma for distinction when reading
		buffer.write(Integer.toString((int)ySize));
		buffer.newLine(); 
		
		for (Entities d : arrOfDrones) {
			if(d instanceof Drone){
				buffer.write(Integer.toString((int)d.getX())); 
				buffer.write(",");								//Reads X,Y,RAR
				buffer.write(Integer.toString((int)d.getY())); 
				buffer.write(",");
				buffer.write(Integer.toString((int)d.getRad()));
				buffer.newLine();	
			}
		}
		buffer.close();
    }
    
    void loadFile() throws IOException {
    			JFileChooser jFC = new JFileChooser("C:\\Users\\Derin\\eclipse-workspace");
    			int uSelect = jFC.showOpenDialog(null); 
    			if (uSelect == JFileChooser.APPROVE_OPTION) {
    				File userFile = jFC.getSelectedFile(); 
    				if (jFC.getSelectedFile().isFile()) { 
    					readFile(userFile); 
    				}
    			}
    }
    
    void readFile(File fileToRead) throws IOException {
    	String data = " ";
		FileReader fileReader = new FileReader(fileToRead);
		BufferedReader reader = new BufferedReader(fileReader);
		data = reader.readLine(); //Reads one character at time, stores in data
		String[] splitArgs = data.split(","); //Text is split into parameters by commas
		if (!arrOfDrones.isEmpty()) {
			arrOfDrones.clear(); //Clear existing drones, if there are any
		}
		
		while (data != null) {
			data = reader.readLine();
			String[] numbers = data.split(","); //Split X,Y,DIRECTION 
			int x = Integer.parseInt(numbers[0]); //Like arena, index 0 is x
			int y = Integer.parseInt(numbers[1]); //Index 1 is y
			int rad = Integer.parseInt(numbers[2]);
			arrOfDrones.add(new Drone(x, y, rad, 25, 5)); //With gathered data on drone, add it to list
		}
		reader.close();
    }

}

