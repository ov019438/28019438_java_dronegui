package JavaGUI;

/**
 *  Obstacle class, extends upon Entities
 */

public class Obstacle extends Entities{
	double angle, speed;			

	public Obstacle() {
	}


	public Obstacle(double ix, double iy, double ir) {
		super(ix, iy, ir);
	}


	@Override
	protected void checkDrone(DroneArena da) { // override to provide new implementation of existing method
		angle = da.CheckDroneAngle(x, y, rad, angle, droneID);
	}


	@Override
	protected void adjustDrone() {
		double newAngle = angle*Math.PI/180;		
		double newX = x + speed*Math.cos(newAngle);
		double newY = y + speed*Math.sin(newAngle);
		x = newX;		
		y = newY;	
	}
	
	protected String getStrType() {
		return "Obstacle";
	}


}
