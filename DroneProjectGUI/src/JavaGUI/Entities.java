package JavaGUI;

/**
 * Entities class, protects key attributes for obstacle and drone
 * @author Derin Aya
 *
 */

public abstract class Entities {
	protected double x, y, rad;			
										
	static int droneCount = 0;		
	protected int droneID;				
	/**
	 * 
	 */
	public Entities() {
		this(50, 50, 25);
	}
	
	/**
	 * Constructs a specific type of entity
	 * @param ix	
	 * @param iy	
	 * @param ir	
	 */
	public Entities(double ix, double iy, double ir) {
		x = ix;
		y = iy;
		rad = ir;
		droneID = droneCount++;	
	}
	
	/**
	 * return x position
	 * @return
	 */
	public double getX() { return x; }
	
	/**
	 * return y position
	 * @return
	 */
	public double getY() { return y; }
	
	/**
	 * return radius of drone
	 * @return
	 */
	public double getRad() { return rad; }
	
	/** 
	 * setter function for drone object with given coords
	 * @param nx
	 * @param ny
	 */
	public void setXY(double nx, double ny) {
		x = nx;
		y = ny;
	}
	
	/**
	 * returns the drone's ID
	 * @return
	 */
	public int getID() {return droneID; }
	
	/**
	 * draws a drawn on the canvas at given coordinates
	 */
	
	public void drawDrone(MyCanvas mc) {
		mc.showDrone(x, y, rad);
	}
	
	/**
	 * draws a drone on the canvas at given coordinates
	 */
	
	protected String getStrType() {
		return "Entities";
	}
	
	
	
	/** 
	 * return string describing drone
	 */
	public String toString() {
		return getStrType()+" at "+Math.round(x)+", "+Math.round(y);
	}
		
	/**
	 * abstract method for checking a drone in arena d
	 * @param d 
	 */
	protected abstract void checkDrone(DroneArena d);
	
	/**
	 * abstract method for adjusting a drone
	 */
	protected abstract void adjustDrone();
	
	/**
	 * Checks for collisions, be it with a wall or another drone, or an obstacle 
	 * @param ox
	 * @param oy
	 * @param or
	 * @return true (if collision)
	 */
	
	public boolean collision(double ox, double oy, double or) {
		return (ox-x)*(ox-x) + (oy-y)*(oy-y) < (or+(2*rad))*(or+(2*rad)); // collision is detected if distance between drone and ox, oy is less than rad
	}		// collision if distance between drone and ox,oy < ist rad + or
	
	/** 
	 * checks for collision between two drones
	 * @param oDrone (passed drone)
	 * @return true if collision
	 */
	public boolean collision (Entities oDrone) {
		return collision(oDrone.getX(), oDrone.getY(), oDrone.getRad());
	}

}

