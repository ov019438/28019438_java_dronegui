package JavaGUI;

/**
 *  DroneInterface, acts as the main graphical user interface for the program
 */

import java.io.IOException;
import java.util.ArrayList;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class DroneInterface extends Application {
	private MyCanvas gc;
	private AnimationTimer timer;								
	private VBox rtPane;										
	private DroneArena arena;

	/**
	 * sets titles for GUI menus
	 */
	
	private void setTitles() {
	    Alert alert = new Alert(AlertType.INFORMATION);	// sets the titles for the sim menu
	   alert.setTitle("About");									
	    alert.setHeaderText(null); // and about menu
	    alert.setContentText("This is a drone simulator");			
	    alert.showAndWait();										
	}
	
	 /**
	  * set up the mouse event to allow user to click buttons
	  * @param canvas
	  */
	
	void setMouseEvents (Canvas canvas) {
	       canvas.addEventHandler(MouseEvent.MOUSE_PRESSED, // sets up event - user click
	    	       new EventHandler<MouseEvent>() {
	    	           @Override
	    	           public void handle(MouseEvent e) {
	  		            	updateC();							
	  		            	updateInfo();
	    	           }
	    	       });
	}
	/**
	 * Sets up menu and buttons for GUI, as well as handling functionality like save and load
	 * @return
	 */
	
	MenuBar setMenu() {
		MenuBar menuBar = new MenuBar();						// create menu
		Menu mFile = new Menu("File");							
		MenuItem mSave = new MenuItem("Save");					
		mSave.setOnAction(new EventHandler<ActionEvent>() {
		    public void handle(ActionEvent t) {
		    	try {
					arena.saveFile();				// calls savefile from area to save
				} catch (IOException e) {
					e.printStackTrace(); // else print stack error
				}											 	
		    }
		});
		MenuItem mLoad = new MenuItem("Load");
		mLoad.setOnAction(new EventHandler<ActionEvent>() {
		    public void handle(ActionEvent t) {
		    	try {
					arena.loadFile();				// same again but to load
				} catch (IOException e) {
					e.printStackTrace();
				}											 	
		    }
		});
		MenuItem mExit = new MenuItem("Exit");					
		mExit.setOnAction(new EventHandler<ActionEvent>() {
		    public void handle(ActionEvent t) {					
	        	timer.stop(); //stop animation timer									
		        System.exit(0);	//kill program							
		    }
		});
		mFile.getItems().addAll(mSave, mLoad, mExit);	// add elements to menu
		Menu mHelp = new Menu("Help");							
		MenuItem mAbout = new MenuItem("About");				
		mAbout.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
            	setTitles();									
            }	
		});
		mHelp.getItems().addAll(mAbout);						
		menuBar.getMenus().addAll(mFile, mHelp);				
		return menuBar;											
	}

	/**
	 * creates the panel that will contain and lay the buttons
	 * @return
	 */
	private HBox setButtons() {
	    Button btnStart = new Button("Start sim");	// when pressed, start sim				
	    btnStart.setOnAction(new EventHandler<ActionEvent>() {	
	        @Override
	        public void handle(ActionEvent event) {
	        	timer.start();									// start animation timer (simulation starts)
	       }
	    });

	    Button btnStop = new Button("Pause sim"); //pauses sim					
	    btnStop.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	           	timer.stop();	// if pressed, pause timer
	       }
	    });
	    
	    Button btnReset = new Button("Clear sim");		
	    btnReset.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	        	timer.start(); 
	        	arena.resetArena();		// resets all entities in the arena			
	       }
	    });
	    

	    Button btnAddLow = new Button("Spawn Drone");				// spawn a drone entity within the world
	    btnAddLow.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	           	arena.addDrone('d');									// call add from arena
	           	updateC();											// update state
	       }
	    });
	    
	    Button btnAddObs = new Button("Spawn Obstacle");						
	    btnAddObs.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	           	arena.addDrone('o');									
	           	updateC();											
	       }
	    });
	    
	    Button btnSpeedup = new Button("Increase Speed");					// calls arena method to increase speed	
	    btnSpeedup.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	           	arena.increaseSpeed();										
	       }
	    });
	    
	    Button btnSpeeddown = new Button("Decrease Speed");						// button to add another obstacle
	    btnSpeeddown.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	           	arena.decreaseSpeed();										// redraws the arena to add it in
	       }
	    });
	    
	  
	    return new HBox(btnStart, btnStop, btnReset, new Label("  "), btnAddLow, btnAddObs, new Label("  "), btnSpeeddown, btnSpeedup); //set the buttons
	}

	
	/** 
	 * updates stage using graphics context
	 */
	
	public void updateC() { // update the stage
	 	gc.clearCanvas();		
	 	arena.drawArena(gc);
	}
	
	/**
	 * presents info sent by arena, such as drone loc and direction
	 */
	
	public void updateInfo() {
		rtPane.getChildren().clear();				// right panel
		ArrayList<String> allDrones = arena.describeAll();
		for (String s : allDrones) {
			Label l = new Label(s); 		// send info to label
			rtPane.getChildren().add(l);	// add label	
		}	
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("28019438 Drone Simulation");
	    BorderPane bp = new BorderPane();
	    bp.setPadding(new Insets(10, 20, 10, 20));
	    bp.setTop(setMenu());											
  	   
	    Group root = new Group();										// create group with canvas
	    Canvas canvas = new Canvas(560, 400 );
	    root.getChildren().add( canvas );
	    bp.setLeft(root);												// sets canvas on left
	
	    gc = new MyCanvas(canvas.getGraphicsContext2D(), 800, 500);

	    setMouseEvents(canvas);											// sets stage mouse events

	    arena = new DroneArena(400, 360);								// defines arena
	    updateC(); // updates canvas
	    
	    timer = new AnimationTimer() {									// animation time will handle main stage loop
	        public void handle(long currentNanoTime) {					
	        		arena.checkDrones();								
		            arena.adjustDrones();								// move all drones
		            updateC();										// update
		            updateInfo();										// display drone and obstacle info
	        }
	    };

	    rtPane = new VBox();											
		rtPane.setAlignment(Pos.TOP_LEFT);								
		rtPane.setPadding(new Insets(5, 75, 75, 5));					
 		bp.setRight(rtPane);											
 		bp.setBottom(setButtons());										// set botton panel up with buttons

	    Scene scene = new Scene(bp, 800, 600);			// set up scene size
        bp.prefHeightProperty().bind(scene.heightProperty());
        bp.prefWidthProperty().bind(scene.widthProperty());

        primaryStage.setScene(scene);
        primaryStage.show();
	}

	/**
	 * load everything in
	 * @param args
	 */
	
	public static void main(String[] args) {
	    Application.launch(args);			// launch the GUI

	}
}

