package JavaGUI;

/**
 * Drone class, used for building and moving drone
 * @author Derin Aya
 *
 */

public class Drone extends Entities {
	double speed, angle; // Speed and angle for drone
	
	/**
	 * 
	 */
	public Drone() {
	}

	/** Drone
	 * constructor for drone, size=ir, x=ix, y=iy, ia=angle, speed=is
	 * @param ix
	 * @param iy
	 * @param ir
	 * @param ia
	 * @param is
	 */
	
	public Drone(double ix, double iy, double ir, double ia, double is) {
		super(ix, iy, ir);
		angle = ia;
		speed = is;
	}
	
	/** 
	 * toString
	 * Returns info on drone
	 */
	
	public String toString() {
		return "[" + getStrType()+" " + droneID +" at "+Math.round(x)+", "+Math.round(y) + ", with speed " + Math.round(speed) +   
				" \n and travelling at angle " + Math.round(angle) + "]"; // Build string with details on drone
	}

	/**
	 * checkDrone  
	 * changes angle if collision
	 * @param da  
	 */
	
	protected void checkDrone(DroneArena da) {
		angle = da.CheckDroneAngle(x, y, rad, angle, droneID); //Used to check if a collision occurs
	}

	/**
	 * adjustDrone
	 * Used to move drone, gives new coords modified with angle
	 */

	protected void adjustDrone() {
		double newAngle = angle*Math.PI/180;		// Convert to radians 
		double newX = x + speed*Math.cos(newAngle); // Give new coords using angle generated
		double newY = y + speed*Math.sin(newAngle);
		x = newX;		
		y = newY;	
	}
	
	/**
	 * getStrType
	 * return string describing drone type
	 */
	
	protected String getStrType() {
		return "Drone"; // Returns drone type
	}
}

